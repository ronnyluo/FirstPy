
import time
from appium import webdriver

LOGIN_NAME = '13507460132'
LOGIN_PWD = '123456'

WAIT_TIME_OUT = 60 #超时时间 单位秒
WAIT_INTERVAL = 1 #等待间隔 即多久检测一次页面 单位秒

WAIT_TIME_SHORT = 1
WAIT_TIME_DEFAULT = 3
WAIT_TIME_LONG = 5

KEY_TAKE_PHOTO = 27

ACTIVITY_MAIN = '.MainActivity'
ACTIVITY_ACCOUNT = '.account.AccountMainActivity'

def start_auto_test():
	try:
		driver = fun_test_init()
		print('初始化完成')

		fun_eenterprise_start(driver)

		fun_wait_activity(driver)

	except Exception as e:
		raise Exception(e)
	else:
		print('测试顺利完成')
		pass
	finally:
		pass	

#-------------非业务层--------------------

#测试程序初始化
def fun_test_init():
	try:
		desired_caps = {}
		desired_caps['platformName'] = 'Android'
		desired_caps['deviceName'] = '127.0.0.1'
		desired_caps['noReset'] = 'false'#不需要再次安装
		desired_caps['appPackage'] = 'com.onebank.moa'
		desired_caps['appActivity'] = 'com.onebank.moa.SplashActivity'
		#desired_caps['appActivity'] = '.TestMainActivity'
		driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
		return driver
		pass
	except Exception as e:
		raise Exception(e)
	else:
		pass
	finally:
		pass

#休眠duration秒
def fun_sleep(duration):
	time.sleep(duration)
	pass

#获得屏幕宽度
def fun_get_screen_width(driver):
	return driver.get_window_size()['width']

#获得屏幕高度
def fun_get_screen_height(driver):
	return driver.get_window_size()['height']

#等待页面跳转完成
def fun_wait_activity(driver, name, time_out, interval):
	return driver.wait_activity(name, time_out, interval)

#页面滑动 ps:duration默认5毫秒
def fun_swipe(driver, startx, starty, endx, endy, duration):
	return driver.swipe(startx, starty, endx, endy, duration)

#页面点击 最多五个点
def fun_tap(driver, pointArray, duration):
	return driver.tap(pointArray, duration)

#获得页面元素
def fun_get_element_by_id(driver, elementId):
	try:
		return driver.find_element_by_id(elementId)
		pass
	except Exception as e:
		#raise Exception('No element\'s id is' + str(elementId))
		pass

#获得页面元素
def fun_get_element_by_accessibility_id(driver, accessibilityId):
	try:
		return driver.find_element_by_accessibility_id(accessibilityId)
		pass
	except Exception as e:
		#raise Exception('No element\'s accessibility id is' + str(accessibilityId))
		pass

#获得页面元素
def fun_get_element_by_class_name(driver, className):
	try:
		return driver.find_element_by_class_name(className)
		pass
	except Exception as e:
		#raise Exception('No elements\'s class name is' + str(className))
		pass

#获得页面元素
def fun_get_elements_by_class_name(driver, className):
	try:
		return driver.find_elements_by_class_name(className)
		pass
	except Exception as e:
		#raise Exception('No elements\'s class name is' + str(className))
		pass

#页面元素点击
def fun_click_element_by_id(driver, elementId):
	element = fun_get_element_by_id(driver, elementId)
	if element != None:
		element.click()
		pass
	else:
		print(str(elementId) + ' is none')
		pass
	pass

#页面元素点击
def fun_set_text_for_element_by_id(driver, elementId, content):
	element = fun_get_element_by_id(driver, elementId)
	if element != None:
		element.send_keys(content)
		pass
	else:
		print(str(elementId) + ' is none')
		pass
	pass

#-------------非业务层--------------------

def fun_eenterprise_start(driver):
	flag = True

	while flag:
		
		fun_sleep(WAIT_TIME_DEFAULT)
		if (driver.current_activity == ACTIVITY_ACCOUNT):
			flag = False
			fun_login(driver)
			pass
		else:
			print('停留在了：' + driver.current_activity)
			pass
		pass

		pass

	pass

def fun_login(driver):
	if driver.current_activity == ACTIVITY_ACCOUNT:
		fun_set_text_for_element_by_id(driver, 'com.onebank.moa:id/et_phonenumber_inputbox', LOGIN_NAME)
		fun_set_text_for_element_by_id(driver, 'com.onebank.moa:id/et_password_inputbox', LOGIN_PWD)
		fun_click_element_by_id(driver, 'com.onebank.moa:id/btn_checkphone_submit')
		pass

def fun_open_account(driver):
	fun_click_element_by_id(driver, 'com.onebank.moa:id/tab_text_find')
	pass

def fun_close_company(driver):
	pass

#-------------脚本启动--------------------

if __name__ == '__main__':
	start_auto_test() 
