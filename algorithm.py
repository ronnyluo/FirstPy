import logging

costs = {}
processed = [10]
infinity = float("inf")
parents = {}
graph = {"S": {"A": 5, "B": 2}
    , "A": {"C": 4, "D": 2}
    , "B": {"A": 8, "D": 7}
    , "C": {"D": 6, "E": 3}
    , "D": {"E": 1}}
# graph = {}
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s : %(levelname)s : %(message)s',
    filemode='w',
)


def fun_init_cache():
    # graph["S"] = ["A", "B"]
    # graph["A"] = ["C", "D"]
    # graph["B"] = ["A", "D"]
    # graph["C"] = ["D", "E"]
    # graph["D"] = ["E"]
    # graph["S"] = {}
    # graph["S"]["A"] = 5
    # graph["S"]["B"] = 2
    # graph["A"] = {}
    # graph["A"]["C"] = 4
    # graph["A"]["D"] = 2
    # graph["B"] = {}
    # graph["B"]["A"] = 8
    # graph["B"]["D"] = 7
    # graph["C"] = {}
    # graph["C"]["D"] = 6
    # graph["C"]["E"] = 3
    # graph["D"] = {}
    # graph["D"]["E"] = 1
    # print("初始化graph表完成")
    logging.debug("初始化graph表完成")
    # logging.debug(graph)
    logging.debug(graph)
    costs["A"] = 5
    costs["B"] = 2
    costs["C"] = infinity
    costs["D"] = infinity
    costs["E"] = infinity
    logging.debug("初始化costs表完成")
    parents["A"] = "S"
    parents["B"] = "S"
    logging.debug("初始化parents表完成")


def find_lowest_cost_way():
    node = find_lowest_cost_node(costs)
    print("node=" + node)
    while node is not None and node is not "E":
        cost = costs[node]
        neighbors = graph[node]
        for n in neighbors.keys():
            new_cost = cost + neighbors[n]
            if costs[n] > new_cost:
                costs[n] = new_cost  # 更新costs中n这个节点的花费
                parents[n] = node  # 同时更新parent
        processed.append(node)
        node = find_lowest_cost_node(costs)
    logging.info("花费最少为：" + str(costs["E"]))
    # print("花费最少为"+str(costs["E"]))
    logging.info("花费最少的路径：" + get_lowest_way())


def get_lowest_way():
    lowest_way = []
    node = parents.get("E")
    lowest_way.append("E")
    while node is not None:
        lowest_way.insert(0, node)
        node = parents.get(node)
    return lowest_way.__str__()


def find_lowest_cost_node(costs):
    lowest_cost = infinity
    lowest_cost_node = None
    for node in costs:
        cost = costs[node]
        if cost < lowest_cost and node not in processed:
            lowest_cost = cost
            lowest_cost_node = node
    return lowest_cost_node


if __name__ == '__main__':
    fun_init_cache()
    find_lowest_cost_way()
