import time
from urllib import request
import json
from bs4 import BeautifulSoup


# 获取所有标签
def fun_get_movie_tags():
    url = 'https://movie.douban.com/j/search_tags?type=movie'
    response = request.urlopen(url)
    result = response.read()
    result = json.loads(result)
    tags = result['tags']
    for tag in tags:
        print(tag)
    return tags


# https://movie.douban.com/j/search_subjects?type=movie&tag=%E7%83%AD%E9%97%A8&sort=recommend&page_limit=20&page_start=0

def get_movies(tags):
    movies = []
    for tag in tags:
        start = 0
        url = 'https://movie.douban.com/j/search_subjects?type=movie&tag=' + tag + '&sort=recommend&page_limit=20&page_start=' + str(
            start)
        print(url)
        url = request.quote(url, "/:?=&", "utf-8")
        response = request.urlopen(url)
        result = response.read()
        result = json.loads(result)
        result = result["subjects"]
        # 如果没有数据了就停止循环请求数据
        if result is None or len(result) == 0:
            break

        for item in result:
            movies.append(item)
        start += 20
        time.sleep(0.5)
        break
    print("总共获取了：" + str(len(movies)) + "部电影！")
    return movies


def get_movie_detail(movies):
    for movie in movies:
        url = movie["url"]
        print(url)
        response = request.urlopen(url)
        result = response.read()
        # result.decode('utf-8')
        html = BeautifulSoup(result, "html.parser")
        # 提取这个html页面中的电影详情
        # 捕获异常，有道电影详情页面中并没有简介
        try:
            # 尝试提取电影详情
            description = html.find_all("span", attrs={"property": "v:summary"})[0].get_text()
        except Exception as  e:
            print(movie['title']+"description为空")
            movie['description'] = ""
        else:
            movie['description'] = description
        finally:
            pass
        time.sleep(0.5)
        # break
    print("完成详细信息的抓取")
    print("movies的信息" + str(movies))


def write_to_file(movies):
    fw = open('douban_movies.txt', 'w')
    fw.write('title+rate^url+cover+id^description\n')
    for item in movies:
        print('已写入'+str(item))
        fw.write(item['title'] + '+' + item['rate'] + '+' + item['url'] + '+' + item['cover'] + '+' + item['id'] + '+' +
                 item['description'] + '\n')
    fw.close()
    pass


if __name__ == "__main__":
    tags = fun_get_movie_tags()
    movies = get_movies(tags)
    get_movie_detail(movies)
    write_to_file(movies)
